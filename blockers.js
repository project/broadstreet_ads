/**
 * @file
 * Custom JS for the Broadstreet Ads module for handling ad blockers.
 */

(function ($) {

Drupal.behaviors.broadstreetAds = {
  attach: function (context, settings) {
    // If no settings were loaded then just bail, something is wrong.
    if (typeof settings.broadstreet_ads === 'undefined') {
      return;
    }
    else if (typeof settings.broadstreet_ads.zones === 'undefined') {
      return;
    }

    // Get the list of zones. If no zones were output there's nothing to do.
    var zones = settings.broadstreet_ads.zones;
    if (zones.length === 0) {
      return;
    }

    // Look for a redirect option, fail if nothing found.
    if (typeof settings.broadstreet_ads.b_option === 'undefined') {
      return;
    }

    // Work out what type of option was requested.
    var option = settings.broadstreet_ads.b_option;

    // Look for different ways the ads would be loaded.
    var ads = 0;

    // Standard blocks, at least how core themes output them.
    ads = ads + $('.block-broadstreet-ads').length;

    // Panels panes use a different class name.
    ads = ads + $('.broadstreet-ad').length;

    // If no ads were loaded, or the Broadstreet JS object was not found,
    // the ads were blocked so perform the action.
    if (ads === 0 || typeof window.broadstreet === 'undefined') {
      // Optional body class.
      if (typeof settings.broadstreet_ads.add_body_class !== 'undefined') {
        if (settings.broadstreet_ads.add_body_class != '') {
          $('body').addClass(settings.broadstreet_ads.add_body_class);
        }
      }

      if (option === 'redirect') {
        var redirect = settings.broadstreet_ads.b_redirect;
        if (redirect === '') {
          return;
        }
        else {
          window.location.href = redirect;
        }
      }
      else if (option === 'popup') {
        var popup = settings.broadstreet_ads.b_popup;
        if (popup === '') {
          return;
        }
        else {
          alert(popup);
        }
      }
      else if (option === 'html') {
        var html = settings.broadstreet_ads.b_html;
        if (html === '') {
          return;
        }
        else {
          var insert = settings.broadstreet_ads.b_insert;
          var remove = settings.broadstreet_ads.b_remove;
          $(insert).prepend(html);
          if (remove !== '') {
            $(remove).remove();
          }
        }
      }
    }
  }
};

})(jQuery);
