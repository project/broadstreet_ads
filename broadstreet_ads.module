<?php
/**
 * @file
 * Primary hook implementations for Broadstreet Ads.
 */

/**
 * Implements hook_init().
 */
function broadstreet_ads_init() {
  // Don't laod anything on admin pages.
  if (path_is_admin(current_path())) {
    return;
  }
  // If one of the first three URL segments is "ajax" then this is an AJAX
  // request and should be ignored. Most paths will be in the format
  // "[modulename]/ajax/something" but e.g. CTools has "ctools/context/ajax".
  elseif (arg(0) == 'ajax' || arg(1) == 'ajax' || arg(2) == 'ajax') {
    return;
  }

  // Don't load anything if there are no zones configured.
  $zones = variable_get('broadstreet_ads_zones', array());
  if (empty($zones)) {
    return;
  }

  // Load the ads initializer.
  $element = array(
    '#type' => 'markup',
    '#markup' => "<script type=\"text/javascript\" src=\"//cdn.broadstreetads.com/init.js\"></script>\n",
  );
  drupal_add_html_head($element, 'broadstreet_ads');
}

/**
 * Implements hook_permission().
 */
function broadstreet_ads_permission() {
  return array(
    'administer broadstreet ads' => array(
      'title' => 'Administer Broadstreet Ads',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function broadstreet_ads_menu() {
  $items = array();

  $items['admin/config/services/broadstreet-ads'] = array(
    'title' => 'Broadstreet Ads',
    'description' => 'Configure the available Broadstreet Ads ad blocks.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('broadstreet_ads_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer broadstreet ads'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'broadstreet_ads.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function broadstreet_ads_block_info() {
  $blocks = array();

  // The core block integration can be disabled.
  if (variable_get('broadstreet_ads_blocks', TRUE)) {
    // Load all of the zones.
    $zones = variable_get('broadstreet_ads_zones', array());

    // Add a block for each zone.
    if (!empty($zones) && is_array($zones)) {
      foreach ($zones as $zone_id => $label) {
        $blocks[$zone_id] = array(
          'info' => broadstreet_ads_zone_label($zone_id),
        );
      }
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function broadstreet_ads_block_view($delta) {
  // Don't do anything if this zone isn't actually active.
  $zones = variable_get('broadstreet_ads_zones', array());
  if (!isset($zones[$delta])) {
    watchdog('broadstreet_ads', 'Zone "@zone" does not exist but the block was requested.', array('@zone' => $delta));
    return;
  }

  // Keep a copy of the zones for later.
  $ads = &drupal_static('broadstreet_ads_zones', array());
  $ads[$delta] = $delta;

  $block = array();
  $block['subject'] = '';
  $block['content'] = array(
    '#theme' => 'broadstreet_ad',
    // '#account_id' => variable_get('broadstreet_ads_account_id'),
    '#zone' => $delta,
    '#pid' => 0,
  );

  return $block;
}

/**
 * Implements hook_js_alter().
 *
 * Store a list of the zones in a JS setting.
 */
function broadstreet_ads_js_alter(&$js) {
  // Skipping admin paths.
  if (path_is_admin(current_path())) {
    return;
  }
  // If one of the first three URL segments is "ajax" then this is an AJAX
  // request and should be ignored. Most paths will be in the format
  // "[modulename]/ajax/something" but e.g. CTools has "ctools/context/ajax".
  elseif (arg(0) == 'ajax' || arg(1) == 'ajax' || arg(2) == 'ajax') {
    return;
  }

  // Load the ad zones.
  $ads = &drupal_static('broadstreet_ads_zones', array());
  $settings = array(
    'broadstreet_ads' => array(
      'zones' => array_keys($ads),
    ),
  );

  // The optional ad blocker blocker code.
  $option = variable_get('broadstreet_ads_blocker_option');

  // Optional body class.
  $body_class = variable_get('broadstreet_ads_blocker_add_body_class', '');

  // Only bother with the following if the body class or option were defined.
  if (!empty($option) || !empty($body_class)) {
    $settings['broadstreet_ads']['b_option'] = $option;
    if ($option == 'redirect') {
      $settings['broadstreet_ads']['b_redirect'] = check_plain(variable_get('broadstreet_ads_blocker_redirect', ''));
    }
    elseif ($option == 'popup') {
      $settings['broadstreet_ads']['b_popup'] = check_plain(variable_get('broadstreet_ads_blocker_popup', ''));
    }
    elseif ($option == 'html') {
      $html = variable_get('broadstreet_ads_blocker_html', array());
      if (!is_array($html) || empty($html['value']) || empty($html['format'])) {
        return;
      }
      $args = array(
        'message' => check_markup($html['value'], $html['format']),
      );
      $settings['broadstreet_ads']['b_html'] = theme('broadstreet_ad_message', $args);
      $settings['broadstreet_ads']['b_insert'] = check_plain($html['insert']);
      $settings['broadstreet_ads']['b_remove'] = check_plain($html['remove']);
    }
    // If it was a different value, only continue if the add_class setting is
    // enabled.
    elseif (empty($body_class)) {
      return;
    }

    if (!empty($body_class)) {
      $settings['broadstreet_ads']['add_body_class'] = check_plain($body_class);
    }

    $path = drupal_get_path('module', 'broadstreet_ads') . '/blockers.js';

    // Get the weight of the last JS item.
    $keys = array_keys($js);
    $key = array_pop($keys);
    $weight = $js[$key]['weight'];

    // Add the custom JS file.
    $js[$path] = drupal_js_defaults($path);
    $js[$path]['weight'] = $weight + 0.001;
  }

  $js['settings']['data'][] = $settings;
}

/**
 * Implements hook_theme().
 */
function broadstreet_ads_theme() {
  return array(
    'broadstreet_ad' => array(
      'template' => 'broadstreet-ad',
      'variables' => array(
        'pid' => 0,
        'zone' => NULL,
      ),
    ),
    'broadstreet_ad_message' => array(
      'template' => 'broadstreet-ad-message',
      'variables' => array(
        'message' => NULL,
      ),
    ),
    'broadstreet_ads_settings_form_table' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implementation of hook_ctools_plugin_dierctory() to let the system know
 * where our content_type plugins are.
 */
function broadstreet_ads_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Format a standard label for the ad zones.
 */
function broadstreet_ads_zone_label($zone_id) {
  $zones = variable_get('broadstreet_ads_zones', array());
  $label = '';

  if (isset($zones[$zone_id])) {
    $label = $zones[$zone_id];
    if (is_numeric($label)) {
      $label = '#' . $label;
    }
    $label = t('Broadstreet Ad: !label', array('!label' => $label));
  }
  else {
    $label = t('Unknown zone "!zone_id"', array('!zone_id' => '$zone_id'));
  }

  return $label;
}
