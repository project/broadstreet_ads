Broadstreet Ads
---------------
Display advertisements from the Broadstreet [1] ad provider.


Configuration & usage
--------------------------------------------------------------------------------
One permission ('Administer AdvertServe Ads') is used to control access to the
settings page.

The main settings page is found at:
  Configuration -> Web services -> Broadstreet Ads

Each ad zone from Broadstreet may be added to the settings page. All that is
necessary to insert an ad is to fill in the "zone ID"; the label may be assigned
if necessary. If there is not enough space to fill in all of the necessary zone
IDs, clicking Save will add two more fields to the page.

Placement of the ads can be done via either blocks or Panels panes. In both
cases a separate block or pane will be made available for each ad in the site,
just assign their placement as usual with the repective display system.


Advanced options
--------------------------------------------------------------------------------
By default the module makes each ad available as a separate block, for use with
the normal block display system, Context and Display Suite, etc, and as separate
CTools content_type plugins for use with Panels, Panelizer, etc. When using the
Panels system it can save some confusion by unchecking the "Enable ad blocks"
option on the settings page; this will remove the normal blocks, leaving only
the Panels panes.


Ad Blockers
--------------------------------------------------------------------------------
Some visitors may use an ad blocking plugin on server on their computer to stop
all ads from loading. There are some options that may be used if it is detected
the visitors are stopping the ads from loading:

 1. The browser can be redirected to a different page, e.g. a page requesting
    the visitors sign up for a membership program.

 2. A message can be displayed in an alert box for the visitor to click on.

 3. A message can be displayed at a specific point in the page (configurable
    via standard jQuery selector); a portion of the page may also be removed.
    Combined, these two options may be used to insert a message and remove the
    page's normal content.

Additionally, a CSS class may be added to the page's body tag.

These options may be managed from the settings page.


Credits / Contact
------------------------------------------------------------------------------
Written and maintained by Damien McKenna [1]. Development is sponsored by
The Batavian [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://drupal.org/project/issues/broadstreet_ads


References
------------------------------------------------------------------------------
1: http://www.broadstreetads.com/
2: https://www.drupal.org/u/damienmckenna
3: http://www.thebatavian.com/
