<?php
/**
 * @file
 * Admin functionality for controlling the Broadstreet Ads module.
 */

/**
 * Primary FormAPI callback for building the form.
 */
function broadstreet_ads_settings_form() {
  // API integration.
  // $form['api'] = array(
  //   '#type' => 'fieldset',
  //   '#title' => t('Broadstreet Ads API integration'),
  //   '#description' => t('By using the API integration the module can automatically load the ad position details, rather than having to manually type them in below.'),
  //   '#collapsible' => TRUE,
  //   '#collapsed' => TRUE,
  // );
  // $form['api']['broadstreet_ads_account_id'] = array(
  //   '#type' => 'textfield',
  //   '#title' => t('API token'),
  //   '#default_value' => variable_get('broadstreet_ads_account_id', ''),
  // );

  $form['zones'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ad zones'),
    // '#description' => t(''),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => t('Add a new zone by filling in the blank row(s). Remove an ad zone by clearing out its zone ID value. Ad zone IDs must be numerical.'),
  );
  $form['zones']['existing'] = array(
    // Theme this part of the form as a table.
    '#theme' => 'broadstreet_ads_settings_form_table',
    // Pass header information to the theme function.
    '#header' => array(
      t('Ad zone ID'),
      t('Label for this ad'),
    ),
    '#tree' => TRUE,
    'rows' => array(
      '#tree' => TRUE,
    ),
  );

  $zones = variable_get('broadstreet_ads_zones', array());
  ksort($zones);

  // Display each ad zone.
  if (!empty($zones)) {
    // Add fields for each zone.
    foreach ($zones as $zone_id => $label) {
      $form['zones']['existing'][$zone_id] = array(
        'id' => array(
          '#type' => 'textfield',
          '#default_value' => $zone_id,
        ),
        'label' => array(
          '#type' => 'textfield',
          '#default_value' => $label,
        ),
      );
      $form['zones']['existing']['rows'][] = $form['zones']['existing'][$zone_id];
    }
  }

  // Add fields for adding new items.
  $form['zones']['existing']['new1'] = array(
    'id' => array(
      '#type' => 'textfield',
      '#default_value' => '',
    ),
    'label' => array(
      '#type' => 'textfield',
      '#default_value' => '',
    ),
  );
  $form['zones']['existing']['rows'][] = $form['zones']['existing']['new1'];
  $form['zones']['existing']['new2'] = array(
    'id' => array(
      '#type' => 'textfield',
      '#default_value' => '',
    ),
    'label' => array(
      '#type' => 'textfield',
      '#default_value' => '',
    ),
  );
  $form['zones']['existing']['rows'][] = $form['zones']['existing']['new2'];

  $dest = variable_get('broadstreet_ads_blocker_redirect');
  $message = variable_get('broadstreet_ads_blocker_message');
  $html = variable_get('broadstreet_ads_blocker_html', array('value' => '', 'format' => filter_default_format(), 'insert' => '', 'remove' => ''));
  $form['adblockers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Adblocker handling'),
    '#description' => t('What to do if it is detected that ads are being blocked.'),
    '#collapsed' => empty($dest) && empty($message) && empty($html['value']),
    '#collapsible' => TRUE,
  );
  $form['adblockers']['add_body_class'] = array(
    '#type' => 'textfield',
    '#title' => t('Add a body class'),
    '#default_value' => variable_get('broadstreet_ads_blocker_add_body_class', ''),
    '#description' => t("Appends a CSS class to the page's BODY tag if the ads are being blocked.")
  );
  $form['adblockers']['option'] = array(
    '#type' => 'select',
    '#title' => t('Which option to use'),
    '#options' => array(
      'redirect' => t('Redirect visitors to another URL'),
      'popup' => t('Show a popup message'),
      'html' => t('Insert a message into the page'),
    ),
    '#empty_option' => t("Do not use"),
    '#default_value' => variable_get('broadstreet_ads_blocker_option'),
    '#description' => t('Only one option can be used at a time.'),
  );
  $form['adblockers']['redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect visitors to another URL'),
    '#description' => t('Optionally redirect to another page or path. It is recommended to use either an absolute path (<em>http://www.example.com/some/page</em>) or at least include a leading slash (<em>/some/page</em>), otherwise visitor may not be redirected properly.'),
    '#default_value' => $dest,
    '#states' => array(
      'visible' => array(
        ':input[name="option"]' => array('value' => 'redirect'),
      ),
    ),
  );
  $form['adblockers']['popup'] = array(
    '#type' => 'textfield',
    '#title' => t('Show a popup message'),
    '#description' => t('Optionally show a pop message to visitors if they are blocking ads.'),
    '#default_value' => $message,
    '#states' => array(
      'visible' => array(
        ':input[name="option"]' => array('value' => 'popup'),
      ),
    ),
  );
  $form['adblockers']['html'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="option"]' => array('value' => 'html'),
      ),
    ),
  );
  $form['adblockers']['html']['html'] = array(
    '#type' => 'text_format',
    '#title' => t('Insert a message into the page'),
    '#description' => t('Optionally show a full text message. It may take some customization, i.e. adding CSS to the theme, to make this work as intended.'),
    '#default_value' => $html['value'],
    '#format' => $html['format'],
    // Support for the WYSIWYG / CKEditor.
    '#wysiwyg' => TRUE,
  );
  $form['adblockers']['html']['insert'] = array(
    '#type' => 'textfield',
    '#title' => t('Insert the message at the top of the following DOM structure'),
    '#default_value' => !empty($html['insert']) ? $html['insert'] : 'body',
    '#description' => t('Supports jQuery selectors. To insert at the top of the page the value "body" might suffice. Ensure that the selector only has one matcher, otherwise the message could be displayed multiple times.'),
  );
  $form['adblockers']['html']['remove'] = array(
    '#type' => 'textfield',
    '#title' => t('Remove the following DOM structure'),
    '#default_value' => $html['remove'],
    '#description' => t('Supports jQuery selectors.'),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced options'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );
  $form['advanced']['blocks'] = array(
    '#title' => t('Enable ad blocks'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('broadstreet_ads_blocks', TRUE),
    '#description' => t('Uncheck to disable ad display using the core <a href="!blocks">Block</a> system. Should only be disabled when using <a href="https://drupal.org/project/panels">Panels</a>, <a href="https://drupal.org/project/panelizer">Panelizer</a> or <a href="https://drupal.org/project/panels_everywhere">Panels Everywhere</a> modules to position the ads.', array('!blocks' => url('admin/structure/block'))),
  );

  // Submit button.
  $form['actions'] = array();
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * FormAPI validation callback for broadstreet_ads_settings_form.
 */
function broadstreet_ads_settings_form_validate($form, &$form_state) {
  // Verify the ad zones.
  if (!empty($form_state['values']['existing'])) {
    foreach ($form_state['values']['existing'] as $key => $zone) {
      if (!empty($zone['id']) && !is_numeric($zone['id'])) {
        form_set_error($key . '][id', t('Ad zone IDs must be numerical, i.e. simple numbers.'));
      }
    }
  }
}

/**
 * FormAPI submission callback for broadstreet_ads_settings_form.
 */
function broadstreet_ads_settings_form_submit(&$form, &$form_state) {
  $values = $form_state['values'];

  // Save the account ID.
  // variable_set('broadstreet_ads_account_id', $form_state['values']['account_id']);

  // Update the 'blocks' option.
  variable_set('broadstreet_ads_blocks', $values['blocks']);

  // The various blocker options.
  if (isset($values['option'])) {
    variable_set('broadstreet_ads_blocker_option', $values['option']);
  }
  else {
    variable_del('broadstreet_ads_blocker_option');
  }
  if (isset($values['redirect'])) {
    variable_set('broadstreet_ads_blocker_redirect', $values['redirect']);
  }
  else {
    variable_del('broadstreet_ads_blocker_redirect');
  }
  if (isset($values['popup'])) {
    variable_set('broadstreet_ads_blocker_popup', $values['popup']);
  }
  else {
    variable_del('broadstreet_ads_blocker_popup');
  }
  if (isset($values['html'])) {
    $html = array(
      'value' => isset($values['html']['value']) ? $values['html']['value'] : '',
      'format' => isset($values['html']['format']) ? $values['html']['format'] : '',
      'insert' => !empty($values['insert']) ? $values['insert'] : '',
      'remove' => !empty($values['remove']) ? $values['remove'] : '',
    );
    variable_set('broadstreet_ads_blocker_html', $html);
  }
  else {
    variable_del('broadstreet_ads_blocker_html');
  }
  if (isset($values['add_body_class'])) {
    variable_set('broadstreet_ads_blocker_add_body_class', $values['add_body_class']);
  }
  else {
    variable_del('broadstreet_ads_blocker_add_body_class');
  }

  $zones = array();

  // Save the ad zone data.
  if (!empty($values['existing']['rows'])) {
    foreach ($values['existing']['rows'] as $key => $zone) {
      $zone_id = $zone['id'];
      $zone_label = $zone['label'];

      // Only save zones that have an ID, i.e. allow records to be removed by
      // blanking out the ad zone.
      if (!empty($zone_id)) {
        // Default label.
        if (empty($zone_label)) {
          $zone_label = $zone_id;
        }
        $zones[$zone_id] = $zone_label;
      }
    }

    // Sort the ad zones so they're listed by zone ID.
    if (!empty($zones)) {
      ksort($zones);
    }
  }
  // Save whatever zones exist.
  variable_set('broadstreet_ads_zones', $zones);

  drupal_set_message(t('Ad zones and settings updated.'));
}

/**
 * Theme callback for the form table.
 */
function theme_broadstreet_ads_settings_form_table(&$variables) {
  // Get the useful values.
  $form = $variables['form'];
  $rows = $form['rows'];
  $header = $form['#header'];

  // Setup the structure to be rendered and returned.
  $content = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
  );

  // Traverse each row.  @see element_chidren().
  foreach (element_children($rows) as $row_index) {
    $row = array();
    // Traverse each column in the row.  @see element_children().
    foreach (element_children($rows[$row_index]) as $col_index) {
      // Render the column form element.
      $row[] = drupal_render($rows[$row_index][$col_index]);
    }
    // Add the row to the table.
    $content['#rows'][] = $row;
  }

  // Render the table and return.
  return drupal_render($content);
}
